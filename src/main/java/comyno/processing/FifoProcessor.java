package comyno.processing;

import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FifoProcessor extends Thread implements ITaskProcessor {
	private static final Logger logger = LoggerFactory.getLogger(FifoProcessor.class);
	
	private LinkedBlockingQueue<ITask> tasks;
	
	public FifoProcessor() {
		this.tasks = new LinkedBlockingQueue<>();
		start();
	}
	
	@Override
	public void schedule(ITask task) {
		try {
			tasks.put(task);
		} catch (InterruptedException e) {
			logger.error("Got interrupted while waiting to put task in processor queue", e);
		}
	}
	
	@Override
	public void terminate() {
		interrupt();
	}
	
	@Override
	public void run() {
		while (!isInterrupted()) {
			try {
				ITask task = tasks.take();
				task.execute();
			} catch (Exception e) {
				logger.error("Something bad happened while processing task", e);
			}
		}
	}
}
