package comyno.processing;

import java.util.Collection;

public interface ITaskProcessor {
	void schedule(ITask task);
	
	default void schedule(Collection<ITask> tasks) {
		for (ITask task : tasks) {
			schedule(task);
		}
	}
	
	void terminate();
}
