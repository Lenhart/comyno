package comyno.counting;

public interface ICountingTask {
	ICountingResult execute();
}
