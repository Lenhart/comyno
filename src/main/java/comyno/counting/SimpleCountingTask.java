package comyno.counting;

import comyno.server.ComynoProperties;

/**
 * Counting task that does only basic counting (exact match).
 */
public class SimpleCountingTask implements ICountingTask {
	private class Result implements ICountingResult {
		private long totalCount;
		
		public Result(long totalCount) {
			this.totalCount = totalCount;
		}
		
		public String asString() {
			return "Character is found " + totalCount + " time(s)";
		}
	}
	
	private String data;
	private char matchChar;
	
	public SimpleCountingTask(String data, char matchChar) {
		this.data = data;
		this.matchChar = matchChar;
	}
	
	public SimpleCountingTask(String data) {
		this(data, ComynoProperties.defaultMatchingChar());
	}
	
	public ICountingResult execute() {
		long count = 0L;
		if (data != null && !data.isEmpty()) {
			for (char c : data.toCharArray()) {
				if (c == matchChar) {
					count++;
				}
			}
		}
		return new Result(count);
	}
}
