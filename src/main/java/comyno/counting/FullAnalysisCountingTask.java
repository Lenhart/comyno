package comyno.counting;

import java.text.DecimalFormat;

import comyno.server.ComynoProperties;

/**
 * Counting task that does a more in-depth analysis.
 */
public class FullAnalysisCountingTask implements ICountingTask {
	private class Result implements ICountingResult {
		private long totalCharsCount;
		private long exactMatchCount;
		private long caseInsensitiveMatchCount;
		
		public Result(long totalCharsCount, long exactMatchCount, long caseInsensitiveMatchCount) {
			this.totalCharsCount = totalCharsCount;
			this.exactMatchCount = exactMatchCount;
			this.caseInsensitiveMatchCount = caseInsensitiveMatchCount;
		}
		
		@Override
		public String asString() {
			double exactPercentage = 0.;
			double caseInsensitivePercentage = 0.;
			if (totalCharsCount > 0) {
				exactPercentage = exactMatchCount / (double)totalCharsCount * 100;
				caseInsensitivePercentage = caseInsensitiveMatchCount / (double)totalCharsCount * 100;
			}
			
			DecimalFormat df = new DecimalFormat("0.00");
			
			StringBuilder builder = new StringBuilder();
			builder.append("There is ").append(exactMatchCount).append(" exact matches, which accounts to ");
			builder.append(df.format(exactPercentage)).append("% of all (").append(totalCharsCount).append(") characters.\n");
			builder.append("There is ").append(caseInsensitiveMatchCount).append(" matches, when ignoring the case, which accounts to ");
			builder.append(df.format(caseInsensitivePercentage)).append("% of all characters");
			
			return builder.toString();
		}
	}

	private String data;
	private char matchChar;
	
	public FullAnalysisCountingTask(String data, char matchChar) {
		this.data = data;
		this.matchChar = matchChar;
	}
	
	public FullAnalysisCountingTask(String data) {
		this(data, ComynoProperties.defaultMatchingChar());
	}
	
	public ICountingResult execute() {
		long totalChars = 0L;
		long exactMatches = 0L;
		long insesitiveMatches = 0L;
		
		char lowercaseMatchChar = Character.toLowerCase(matchChar);
		if (data != null && !data.isEmpty()) {
			for (char c : data.toCharArray()) {
				totalChars++;
				if (c == matchChar) {
					exactMatches++;
				} else if (Character.toLowerCase(c) == lowercaseMatchChar) {
					insesitiveMatches++;
				}
			}
		}
		return new Result(totalChars, exactMatches, insesitiveMatches + exactMatches);
	}
}
