package comyno.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import comyno.counting.ICountingTask;
import comyno.counting.SimpleCountingTask;
import comyno.processing.FifoProcessor;
import comyno.processing.ITask;
import comyno.processing.ITaskProcessor;

/**
 * Opens a server socket to which you can telnet to.
 * Then you type text you want to be processed.
 * Finish text with "<<END>>" (omit quotes) to process it and get result.
 */
public class TelnetServer extends Thread {
	/**
	 * This task is in charge of gathering text from client socket's input stream.
	 * It avoids blocking by checking if data is available and only then reads data,
	 * otherwise new task will be created and put in the processing queue so that worker threads can be shared
	 * for multiple client sockets.
	 */
	private class TelnetClientTask implements ITask {
		private Socket clientSocket;
		private BufferedReader in;
		private ITaskProcessor clientProcessor;
		private ITaskProcessor countingProcessor;
		private StringBuilder dataBuilder;
		
		public TelnetClientTask(Socket clientSocket, BufferedReader in, ITaskProcessor clientProcessor,
				ITaskProcessor countingProcessor, StringBuilder dataBuilder) {
			this.clientSocket = clientSocket;
			this.in = in;
			this.clientProcessor = clientProcessor;
			this.countingProcessor = countingProcessor;
			if (dataBuilder == null) {
				dataBuilder = new StringBuilder();
			}
			this.dataBuilder = dataBuilder;
		}
		
		@Override
		public void execute() {
			try {
				boolean done = false;
				while (in.ready()) {
					String line = in.readLine();
					logger.info("Line that is read is [" + line + "]");
					done = "<<END>>".equals(line) || line == null;
					if (!done) {
						dataBuilder.append(line).append("\n");
					}
				}
				
				if (done) {
					countingProcessor.schedule(new ITask() {
						@Override
						public void execute() {
							try {
								ICountingTask countingTask = new SimpleCountingTask(dataBuilder.toString());
								String result = countingTask.execute().asString();
								logger.info("Result is: " + result);
								
								PrintWriter out;
								out = new PrintWriter(clientSocket.getOutputStream());
								
								out.println(result);
								out.flush();
								out.close();
								in.close();
								clientSocket.close();
							} catch (IOException e) {
								logger.error("Failed to close socker", e);
							}
						}
					});
				} else {
					clientProcessor.schedule(new TelnetClientTask(clientSocket, in, clientProcessor, countingProcessor, dataBuilder));
				}
			} catch (Exception e) {
				logger.error("Failed processing client request", e);
			}
		}
	}
	
	private static final Logger logger = LoggerFactory.getLogger(TelnetServer.class);
	
	private int port;
	private ITaskProcessor countingProcessor;
	
	public void start(int port, ITaskProcessor countingProcessor) {
		logger.info("### Starting telnet server on port " + port);
		
		this.port = port;
		this.countingProcessor = countingProcessor;
		
		start();
	}
	
	@Override
	public void run() {
		ServerSocket serverSocket = null;
		ITaskProcessor clientProcessor = null;
		try {
			serverSocket = new ServerSocket(port);
			clientProcessor = new FifoProcessor();
		} catch (Exception e) {
			logger.error("Failed to start the telnet server", e);
			System.exit(1);
		}
		logger.info("### Telnet server started successfully");
		
		while (!isInterrupted()) {
			try {
				Socket clientSocket = serverSocket.accept();
				logger.debug("Accepted new client");
				BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
				clientProcessor.schedule(new TelnetClientTask(clientSocket, in, clientProcessor, countingProcessor, null));
			} catch (IOException e) {
				logger.error("Failed processing client request", e);
			}
		}
		
		clientProcessor.terminate();
		try {
			serverSocket.close();
		} catch (IOException e) {
			logger.error("Failed closing server socket", e);
		}
	}
}
