package comyno.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import comyno.processing.FifoProcessor;
import comyno.processing.ITaskProcessor;
import comyno.server.jetty.JettyServer;

/**
 * Main program. It starts Telnet and Jetty (Http) servers.
 */
public class Main {
	private static final Logger logger = LoggerFactory.getLogger(Main.class);
	
	public static void main(String[] args) {
		try {
			ComynoProperties.initialize();
			ITaskProcessor processor = new FifoProcessor();
			
			TelnetServer telnet = new TelnetServer();
			int telnetPort = ComynoProperties.telnetPort();
			telnet.start(telnetPort, processor);
			
			int jettyPort = ComynoProperties.jettyPort();
			JettyServer jettyServer = JettyServer.start(jettyPort);
			jettyServer.join();
		} catch (Exception e) {
			logger.error("Failed starting process", e);
		}
	}
}
