package comyno.server.jetty;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import comyno.counting.FullAnalysisCountingTask;
import comyno.counting.ICountingResult;
import comyno.counting.ICountingTask;
import comyno.counting.SimpleCountingTask;
import comyno.server.ComynoProperties;

/**
 * Servlet that handles request that have all data passed through http query parameters.
 * Parameter "text" is mandatory, you can optionally use "char" parameter to search for non-default
 * character and "analysis" to get more in-depth results.
 */
public class CountServlet extends HttpServlet {
	private static final long serialVersionUID = 8626099809350368676L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("text/html");
        response.setStatus(HttpServletResponse.SC_OK);
        
        String data = request.getParameter("text");
        char matchingChar = ComynoProperties.defaultMatchingChar();
        if (request.getParameterMap().containsKey("char")) {
        	matchingChar = request.getParameter("char").charAt(0);
        }
        
        ICountingTask task;
        if (request.getParameterMap().containsKey("analysis")) {
        	task = new FullAnalysisCountingTask(data, matchingChar);
        } else {
        	task = new SimpleCountingTask(data, matchingChar);
        }
        
        ICountingResult result = task.execute();
        
        response.getWriter().println("<p>" + result.asString() + "</p>");
	}
}
