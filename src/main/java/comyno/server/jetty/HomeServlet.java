package comyno.server.jetty;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Simple servlet to be used for root path.
 */
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = -8151225936245625361L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
    	response.setContentType("text/html");
        response.setStatus(HttpServletResponse.SC_OK);
        response.getWriter().println("<h1>Available routes are:</h1>");
        response.getWriter().println("<b>/count</b>");
    }
}
