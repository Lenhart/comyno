package comyno.server.jetty;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

/**
 * Wrapper around Jetty's Server to encapsulate configuration.
 */
public class JettyServer extends Server {
	private JettyServer(int port) {
		super(port);
	}
	
	public static JettyServer start(int port) throws Exception {
		JettyServer server = new JettyServer(port);
		
		ServletContextHandler contextHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
		contextHandler.setContextPath("/");
		server.setHandler(contextHandler);
		
		contextHandler.addServlet(new ServletHolder(new HomeServlet()), "/");
		contextHandler.addServlet(new ServletHolder(new CountServlet()), "/count");
		
		server.start();
		return server;
	}
}
