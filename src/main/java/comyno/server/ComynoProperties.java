package comyno.server;

import java.io.FileReader;
import java.util.Properties;

public class ComynoProperties {
	private static Properties properties;
	
	public static void initialize() throws Exception {
		properties = new Properties();
		properties.load(new FileReader("comyno.properties"));
	}
	
	public static char defaultMatchingChar() {
		return properties.getProperty("defaultMatchingChar").charAt(0);
	}
	
	public static int telnetPort() {
		return Integer.parseInt(properties.getProperty("telnetPort"));
	}
	
	public static int jettyPort() {
		return Integer.parseInt(properties.getProperty("jettyPort"));
	}
}
