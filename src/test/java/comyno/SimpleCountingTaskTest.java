package comyno;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import comyno.counting.ICountingResult;
import comyno.counting.ICountingTask;
import comyno.counting.SimpleCountingTask;

@RunWith(Parameterized.class)
public class SimpleCountingTaskTest {
	public String text;
	public char matchChar;
	public String expectedResult;
	
	public SimpleCountingTaskTest(String text, char matchChar, String expectedResult) {
		this.text = text;
		this.matchChar = matchChar;
		this.expectedResult = expectedResult;
	}

	@Parameters
	public static Collection<Object[]> testCases() {
		String template = "Character is found %d time(s)";
		
		return Arrays.asList(new Object[][] {
				{ "Simple test", 'I', String.format(template, 0) },
				{ "Simple test", 'e', String.format(template, 2) },
				{ null, 'e', String.format(template, 0) },
				{ "", 'I', String.format(template, 0) },
				{ "A bit longer TC\nwith new\n\nlines", 'I', String.format(template, 0) },
				{ "A bit longer TC\nwith new\n\nlines", '\n', String.format(template, 3) },
				{ "A bit longer TC\nwith new\n\nlines", ' ', String.format(template, 4) },
				{ "XXXXXXXXXX", 'X', String.format(template, 10) }
		});
	}
	
	@Test
	public void Test() {
		ICountingTask task = new SimpleCountingTask(text, matchChar);
		ICountingResult result = task.execute();
		assertThat(result.asString(), is(equalTo(expectedResult)));
	}
}
