package comyno;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import comyno.counting.FullAnalysisCountingTask;
import comyno.counting.ICountingResult;
import comyno.counting.ICountingTask;

@RunWith(Parameterized.class)
public class FullAnalysisCountingTaskTest {
	public String text;
	public char matchChar;
	public String expectedResult;
	
	public FullAnalysisCountingTaskTest(String text, char matchChar, String expectedResult) {
		this.text = text;
		this.matchChar = matchChar;
		this.expectedResult = expectedResult;
	}
	
	@Parameters
	public static Collection<Object[]> testCases() {
		String template = "There is %d exact matches, which accounts to %s%% of all (%d) characters.\nThere is %d matches, when ignoring the case, which accounts to %s%% of all characters";
		
		return Arrays.asList(new Object[][] {
				{ "Simple test", 'I', String.format(template, 0, "0.00", 11, 1, "9.09") },
				{ "SimplE test", 'e', String.format(template, 1, "9.09", 11, 2, "18.18") },
				{ null, 'e', String.format(template, 0, "0.00", 0, 0, "0.00") },
				{ "", 'I', String.format(template, 0, "0.00", 0, 0, "0.00") },
				{ "A bit longer TC\nwith new\n\nlines", '\n', String.format(template, 3, "9.68", 31, 3, "9.68") },
				{ "A bit longer TC\nwith new\n\nlines", ' ', String.format(template, 4, "12.90", 31, 4, "12.90") },
				{ "XXXXXXXXXX", 'X', String.format(template, 10, "100.00", 10, 10, "100.00") }
		});
	}
	
	@Test
	public void Test() {
		ICountingTask task = new FullAnalysisCountingTask(text, matchChar);
		ICountingResult result = task.execute();
		assertThat(result.asString(), is(equalTo(expectedResult)));
	}
}
