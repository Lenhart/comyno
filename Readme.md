# Comyno demo project

## Problem definition
InfinityEyes D.O.O has slogan "How many I's" is startup offering the best in the world "character I counting service". With this solution user would be able to submit text and see how many times does letter 'I' appear.
No clear expectations, candidate should make all project decisions.

## Idea
Create a service that will count number of I's (or, character specified as parameter) for any way you provide your input text:
- telnet,
- http query parameters,
- json request,
- text file,
- text file (zipped),
- image containing text (parsed to text and then processed),...

## Prerequisites
Java 1.8 (or 1.6+ with project update)
Maven (handles all other dependencies)

## Run
Run Main class from Eclipse or build jar through maven and run it.
Telnet server will by default listen on port 10987 and Http server will by default listen on port 8080.

You can test it using:
- telnet
> curl -v telnet://127.0.0.1:10987
Type your text and to signal you're done type "<<END>>" (omit quotes).
- curl
> curl 'localhost:8080/count?text=TextHere&char=c&analysis=true'
Only parameter <i>text</i> is mandatory, others are optional.

